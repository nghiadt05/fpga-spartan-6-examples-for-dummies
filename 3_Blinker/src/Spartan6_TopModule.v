//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:46:21 09/21/2015 
// Design Name: 
// Module Name:    Spartan6_TopModule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "config.v"

module Spartan6_TopModule #(
	parameter CLK_RATE = 20_000_000, //hz
	parameter CTR_PWM_LEN = 5
	)(
	// global signals
	input 	i_clk,
	input		i_rs_bt,
	output 	o_rst,
	// --------------
		
	// signals for LEDs module
	output 	[3:0] o_led
	// -------------------------	
);
	// mapping signals for reset button module
	Button #(
		.CNT_SIZE(5)
	)Reset_Button(
		.i_clk(i_clk),
		.i_bt(i_rs_bt),
		.o_bt(o_rst)
	);
	// ---------------------------------------
	
	// mapping signals for blinker module
	assign o_led[3] = o_led[0];
	assign o_led[2] = o_led[0];
	assign o_led[1] = o_led[0];
	Blinker #(
		.CLK_RATE(CLK_RATE),
		.SCALE(`FAST)
	)Blinker(
		.i_clk(i_clk),
	 	.i_rst(o_rst),
	 	.o_led(o_led[0])
	);
	// ----------------------------------

endmodule
