`include "config.v"
module Blinker#(
	 parameter CLK_RATE = 20_000_000, //hz 
	 parameter SCALE = `NORMAL
	 )(	 
	 input  	i_clk,
	 input	i_rst,
	 output 	o_led
);
// CNT_SIZE is the bit-length of the counter
parameter CNT_SIZE = $clog2(CLK_RATE*SCALE);
// r_led: buffer register for the output o_led signal 
reg r_led = 1'b0;
assign o_led = r_led;
// C counter 
reg [CNT_SIZE-1:0] C = {CNT_SIZE{1'b0}};

always @(posedge i_clk) begin			
	if(!i_rst)begin	
		C <= {CNT_SIZE{1'b0}};// if reset button is pressed, reset the counter C
		r_led			  <= 1'b0;// and turn the led off
	end else begin	
		C 		<= C + 1'b1; // if reset button is released, keep counting up
		r_led	<= C[CNT_SIZE-1];// and assign the C[n-1] bit for r_led
	end
end
endmodule
