//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:46:21 09/21/2015 
// Design Name: 
// Module Name:    Spartan6_TopModule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Spartan6_TopModule #(
	parameter CLK_RATE = 20_000_000, //hz
	parameter CTR_PWM_LEN = 5
	)(
	// global signals
	input 	i_clk,
	input		i_rs_bt,
	output 	o_rst,
	// --------------
	
	// LED signals
	output	[3:0]	o_led
	// -----------
);
	// mapping signal for reset button module
	Button #(
		.CNT_SIZE(5)
	)Button(
		.i_clk(i_clk),
		.i_bt(i_rs_bt),
		.o_bt(o_rst)
	);
	// --------------------------------------
	
	// assign all LED signals with the value of reset button
	assign o_led = {4{!o_rst}};	// 4 bits, each bit has the value of o_rst
	
endmodule
