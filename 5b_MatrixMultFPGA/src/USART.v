module USART #(
	parameter CLK_RATE = 20000000,
	parameter SERIAL_BAUD_RATE = 9600
	)(
		// global signals
		input i_clk,
		input i_rst,

		// for TX module 
		output 	tx,				// output tx signal
		input 	block,			// block signal sent by outer source to stop transmission(AVR, ...)
		output 	tx_busy,			// output signal to indicate the UART is busy
		input 	[7:0] tx_data,	// input signals that indicate the sending data
		input 	tx_new_data,	// signal to indicate new data is comming
		
		input 	rx,				// serial rx data
		output	[7:0] rx_data,	// output signals from rx module
		output	rx_new_data		// indicate new data that read from rx module
	);
	
  // CLK_PER_BIT is the number of cycles each 'bit' lasts for
  // rtoi converts a 'real' number to an 'integer'
  parameter CLK_PER_BIT = $rtoi($ceil(CLK_RATE/SERIAL_BAUD_RATE));
  
  Serial_tx #(
		.CLK_PER_BIT(CLK_PER_BIT)
  ) Serial_tx (
    .clk(i_clk),
    .rst(i_rst),
    .tx(tx),
    .block(block),
    .busy(tx_busy),
    .data(tx_data),
    .new_data(tx_new_data)
  );
  
  Serial_rx #(
		.CLK_PER_BIT(CLK_PER_BIT)
  ) Serial_rx(
	 .clk(i_clk),
	 .rst(i_rst),
	 .rx(rx),
	 .data(rx_data),
	 .new_data(rx_new_data)
  );  
	
endmodule
