//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:46:21 09/21/2015 
// Design Name: 
// Module Name:    Spartan6_TopModule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "config.v"

module Spartan6_TopModule #(
	parameter CLK_RATE = 20_000_000, //hz
	parameter CTR_PWM_LEN = 8
	)(
	// global signals
	input 	i_clk,
	input		i_rs_bt,
	output 	o_rst,
	// --------------
	
	// signals for Switch module
	input		[3:0]	i_sw, 
	output	[3:0] o_sw_stt,
	// -------------------------
	
	// signals for UART module
	output 	o_tx,	// output tx signal	
	input 	i_rx, // input rx signal
	
	input		i_A_RTS,	// input signal from Arduino indicating data is ready to read
	output   o_F_CTS, // output signal to Arduino indicating that I'm ready to read 
	
	output	o_F_RTS, // output signal to Arduino indicating that my data is ready
	input		i_A_CTS, // input signal from Arduino indicating that it's ready now
	// -------------------------
	
	//debug	
	output	ledout,
	
	// signals for LEDs module (in this example outputs of PWM module are the LEDS)
	output 	[3:0] o_led
	// -------------------------	
);
	// Cnt = 2^BUTTON_CNT_SIZE clock cycles for button debouncing
	parameter BUTTON_CNT_SIZE = 5; 
	
	// mapping signals for reset button module	
	Button #(
		.CNT_SIZE(BUTTON_CNT_SIZE)		
	)Reset_Button(
		.i_clk(i_clk), 
		.i_bt(i_rs_bt),
		.o_bt(o_rst)
	);
	// ---------------------------------------
	
	// mapping signals for blinker module
	// always blink LED 3	
	Blinker #(
		.CLK_RATE(CLK_RATE),
		.SCALE(`NORMAL)
	) Blink_3(
		.i_clk(i_clk),
		.i_rst(o_rst),
		.o_led(o_led[3])
	);
	// ---------------------------------------	
	
	// mapping signal for Matrix Multiplication module
	MatrixMult #(
		.CLK_RATE(CLK_RATE),
		.SERIAL_BAUD_RATE(2400)
	) MatrixMult (
		// global signals
		.i_clk(i_clk),
		.i_rst(o_rst),
		
		// debug
		.ledout(ledout),
		
		// for TX module 
		.tx(o_tx),				// output tx signal
		.rx(i_rx),				// serial rx data
				
		// TX hand-shaking signals
		.i_A_RTS(i_A_RTS),	// input signal from Arduino indicating data is ready to read
		.o_F_CTS(o_F_CTS), 	// output signal to Arduino indicating that I'm ready to read 
	
		.o_F_RTS(o_F_RTS), 	// output signal to Arduino indicating that my data is ready
		.i_A_CTS(i_A_CTS) 	// input signal from Arduino indicating that it's ready now
		
	);
	assign o_led[0] = !o_tx; 	// assign led 0 to tx line
	assign o_led[1] = !i_rx;	// assign led 1 to rx line
	// -------------------------------------------------
			
endmodule




