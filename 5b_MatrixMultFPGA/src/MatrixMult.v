// declare local parameters
`define ARRAY_SIZE 	 5
`define RECEIVE_BUFFER_OFFSET  `ARRAY_SIZE*4
`define RECEIVE_BUFFER_SIZE    2*`ARRAY_SIZE*4 + 1     // ( 2 matrices * (N*N) elements )                   * 4 bytes + 1 check sume byte 
`define SEND_BUFFER_SIZE   	 (1+1)*4 + 1   			 // ( 1 result value + 1 timming value ) * 4 bytes + 1 check sume byte
	
module MatrixMult #(
		parameter CLK_RATE = 20000000,
		parameter SERIAL_BAUD_RATE = 9600
    )(
		// global signals
		input i_clk,
		input i_rst,

		// for TX module 
		output 	tx,				// output tx signal
		input 	rx,				// serial rx data
				
		// TX hand-shaking signals
		input		i_A_RTS,	// input signal from Arduino indicating data is ready to read
		output   o_F_CTS, // output signal to Arduino indicating that I'm ready to read 
		
		output	ledout,
		
		output	o_F_RTS,  // output signal to Arduino indicating that my data is ready
		input		i_A_CTS  // input signal from Arduino indicating that it's ready now	
	
	 );
	 
	 parameter RECEIVE_BUFFER_BIT_SIZE = $clog2(`RECEIVE_BUFFER_SIZE);
	 parameter ARRAY_BIT_SIZE = $clog2(`ARRAY_SIZE);
	 parameter SEND_BUFFER_BIT_SIZE = $clog2(`SEND_BUFFER_SIZE);
	 
	 	
	 localparam STATE_SIZE = 3;
	 localparam IDLE 				= 3'd0, 
					RECEIVE 			= 3'd1, 
					PROCESSING		= 3'd2,		
					PREPARE_DATA	= 3'd3,
					SEND_BACK		= 3'd4;
									
    // variables and signals for UART module
	 wire tx_busy;											// in
	 reg 	tx_new_data;									// out
	 wire rx_new_data;									// in
	 reg  [7:0] tx_data;									// out
	 wire [7:0] rx_data;									// in
	 	 
	 // data buffer for receive and send data
	 reg [7:0]	RECEIVE_BUFFER_d	[0: `RECEIVE_BUFFER_SIZE-1];
	 reg [7:0]	RECEIVE_BUFFER_q	[0: `RECEIVE_BUFFER_SIZE-1];
	 reg [RECEIVE_BUFFER_BIT_SIZE-1:0] iRC_BUFF;
	 
	 reg [7:0]	SEND_BUFFER_d		[0: `SEND_BUFFER_SIZE-1];
	 reg [7:0]	SEND_BUFFER_q		[0: `SEND_BUFFER_SIZE-1];
	 reg [SEND_BUFFER_BIT_SIZE-1:0] iSD_BUFF;
	 	 
	 // For controlling module
	 reg [RECEIVE_BUFFER_BIT_SIZE-1:0] recv_cnt_d, recv_cnt_q = 0;
	 reg [STATE_SIZE-1:0] state_d, state_q = IDLE; 
	 reg [4:0] snd_cnt_d, snd_cnt_q = 0;		 
	 reg [ARRAY_BIT_SIZE:0] array_cnt_d,array_cnt_q = 0;
	 reg isFirstSend = 1'b1;
	 reg isProcessing = 1'b0;	 
	 reg o_F_CTS_reg = 1'b0;
	 reg o_F_RTS_reg = 1'b0;		 	 
	 reg [31:0] SumProduct_d;
	 reg [31:0] SumProduct_q;
	 reg [31:0]	ProcessingTime_d;
	 reg [31:0]	ProcessingTime_q;

	 // assign signals	 
	 //assign tx_data = tx_data_q;	 
	 assign o_F_CTS = o_F_CTS_reg;
	 assign o_F_RTS = o_F_RTS_reg;	
	 
	 // debugging
	 reg led_out_reg = 1'b0;
	 assign ledout = led_out_reg;
	 
	 // mapping to pure UART module (RX and TX)
	 USART #(
		.CLK_RATE(CLK_RATE),
		.SERIAL_BAUD_RATE(SERIAL_BAUD_RATE)
	 ) USART_1 (
		.i_clk(i_clk),
		.i_rst(i_rst),

		// for TX module 
		.tx(tx),								// output tx signal
		.block(1'b0),						// block signal sent by outer source to stop transmission(AVR, ...)
		.tx_busy(tx_busy),				// output signal to indicate the UART is busy
		.tx_data(tx_data),				// input signals that indicate the sending data
		.tx_new_data(tx_new_data),		// signal to indicate new data is comming
		
		.rx(rx),								// serial rx data
		.rx_data(rx_data),				// output signals from rx module
		.rx_new_data(rx_new_data)		// indicate new data that read from rx module
	 );
	 // --------------------------------------
	 
	// Main control program	
	
	always @(*) begin // assychronous block
		// update if nothing is changed
		state_d = state_q;							
		recv_cnt_d = recv_cnt_q;
		array_cnt_d = array_cnt_q;
		snd_cnt_d = snd_cnt_q;
		ProcessingTime_d = ProcessingTime_q;
		SumProduct_d = SumProduct_q;
		
		tx_new_data = 1'b0;
		
		for (iRC_BUFF = 0; iRC_BUFF < `RECEIVE_BUFFER_SIZE; iRC_BUFF = iRC_BUFF + 1)begin
			RECEIVE_BUFFER_d[iRC_BUFF] = RECEIVE_BUFFER_q[iRC_BUFF];
		end
		
		for (iSD_BUFF = 0; iSD_BUFF < `SEND_BUFFER_SIZE; iSD_BUFF = iSD_BUFF + 1)begin
			SEND_BUFFER_d[iSD_BUFF] = SEND_BUFFER_q[iSD_BUFF];
		end
		
		case(state_q)
			IDLE: 
					begin
						led_out_reg = 1'b0;
						recv_cnt_d = 0;  
						array_cnt_d = 0;
						o_F_CTS_reg = 1'b0;
						o_F_RTS_reg = 1'b0;						
						snd_cnt_d = 0;
						isFirstSend = 1'b1;
						ProcessingTime_d = 0;
						isProcessing = 1'b0;	
					   SumProduct_d = 0;
						if(i_A_RTS)begin								
							state_d = RECEIVE;															
						end 						
					end
			RECEIVE: 			
					begin
						//if(i_A_RTS == 1'b1 && !tx_busy)begin	
						if(i_A_RTS == 1'b1)begin
							o_F_CTS_reg = 1'b1;
							if(rx_new_data == 1'b1)begin // new data		
								RECEIVE_BUFFER_d[recv_cnt_q] = rx_data;
																
								//tx_new_data = 1'b1;
								//tx_data = rx_data;
								
								if(recv_cnt_q == `RECEIVE_BUFFER_SIZE-1)begin																
									recv_cnt_d = 0;	
									isProcessing = 1'b1;
									state_d = PROCESSING;									
								end else begin									
									recv_cnt_d = recv_cnt_q + 1;
								end
							end 
						end else begin
							o_F_CTS_reg = 1'b0;							
						end												
					end
			PROCESSING:
					begin									
						// concatnate for each array element
						//led_out_reg = 1'b1;		
						//if(!tx_busy)begin
							//SumProduct_d = SumProduct_q + 2;
							SumProduct_d =			SumProduct_q + 
														({ RECEIVE_BUFFER_q[ 3 + (array_cnt_q<<2) ], 
														  RECEIVE_BUFFER_q[ 2 + (array_cnt_q<<2) ],
														  RECEIVE_BUFFER_q[ 1 + (array_cnt_q<<2) ],
														  RECEIVE_BUFFER_q[ 0 + (array_cnt_q<<2) ]}
														  *															
														 { RECEIVE_BUFFER_q[`RECEIVE_BUFFER_OFFSET + (array_cnt_q<<2) + 3],
														   RECEIVE_BUFFER_q[`RECEIVE_BUFFER_OFFSET + (array_cnt_q<<2) + 2],
														   RECEIVE_BUFFER_q[`RECEIVE_BUFFER_OFFSET + (array_cnt_q<<2) + 1],
														   RECEIVE_BUFFER_q[`RECEIVE_BUFFER_OFFSET + (array_cnt_q<<2) 	]});
							//tx_new_data = 1'b1;
							//tx_data = array_cnt_q<<2;//array_cnt_q;							
							if(array_cnt_q == `ARRAY_SIZE - 1)begin		
								isProcessing = 1'b0;
								//state_d = IDLE;
								state_d = PREPARE_DATA;
								array_cnt_d = 0;
							end else begin
								array_cnt_d = array_cnt_q + 1;
							end
						//end							
					end			
			PREPARE_DATA:
					begin
						SEND_BUFFER_d[0] =  SumProduct_q 		 	& 8'hff;
						SEND_BUFFER_d[1] = (SumProduct_q >> 8)  	& 8'hff;
						SEND_BUFFER_d[2] = (SumProduct_q >> 16) 	& 8'hff;
						SEND_BUFFER_d[3] = (SumProduct_q >> 24)	&8'hff;
						
						SEND_BUFFER_d[4] =  ProcessingTime_q 			& 8'hff;
						SEND_BUFFER_d[5] = (ProcessingTime_q >> 8)   & 8'hff;
						SEND_BUFFER_d[6] = (ProcessingTime_q >> 16)  & 8'hff;
						SEND_BUFFER_d[7] = (ProcessingTime_q >> 24)  & 8'hff;
												
						SEND_BUFFER_d[8] = 	SEND_BUFFER_d[0]^
													SEND_BUFFER_d[1]^
													SEND_BUFFER_d[2]^
													SEND_BUFFER_d[3]^
													SEND_BUFFER_d[4]^
													SEND_BUFFER_d[5]^
													SEND_BUFFER_d[6]^
													SEND_BUFFER_d[7];
											  
						state_d = SEND_BACK;
					end
			SEND_BACK:
					begin
						led_out_reg = 1'b1;
						o_F_RTS_reg = 1'b1;
						if(!tx_busy && i_A_CTS)begin							
							tx_new_data = 1'b1;							
							tx_data = SEND_BUFFER_q[snd_cnt_q];
							snd_cnt_d = snd_cnt_q + 1;
							if(snd_cnt_q == `SEND_BUFFER_SIZE-1)begin								
								state_d = IDLE;
							end else begin
								snd_cnt_d = snd_cnt_q + 1;
							end							
						end	
					end						
			default: 
					begin
						state_d = IDLE;
					end
		endcase
	end
	
	always @(posedge i_clk) begin	// synchronous block
		if(!i_rst) begin
			state_q <= IDLE;
			
			for (iRC_BUFF = 0; iRC_BUFF < `RECEIVE_BUFFER_SIZE; iRC_BUFF = iRC_BUFF + 1)begin
				RECEIVE_BUFFER_q[iRC_BUFF] <= 0;
			end
			
			for (iSD_BUFF = 0; iSD_BUFF < `SEND_BUFFER_SIZE; iSD_BUFF = iSD_BUFF + 1)begin
				SEND_BUFFER_q[iSD_BUFF] <= 0;
			end
		
		end else begin
		
			state_q <= state_d;			
			
			for (iRC_BUFF = 0; iRC_BUFF < `RECEIVE_BUFFER_SIZE; iRC_BUFF = iRC_BUFF + 1)begin
				RECEIVE_BUFFER_q[iRC_BUFF] <= RECEIVE_BUFFER_d[iRC_BUFF];
			end
			
			for (iSD_BUFF = 0; iSD_BUFF < `SEND_BUFFER_SIZE; iSD_BUFF = iSD_BUFF + 1)begin
				SEND_BUFFER_q[iSD_BUFF] <= SEND_BUFFER_d[iSD_BUFF];
			end
			
		end	
		
		recv_cnt_q <= recv_cnt_d;		
		array_cnt_q <= array_cnt_d;
		snd_cnt_q <= snd_cnt_d;
		SumProduct_q <= SumProduct_d;
		if(isProcessing)begin
			ProcessingTime_q <= ProcessingTime_d + 1;
		end else begin
			ProcessingTime_q <= ProcessingTime_d;
		end
	end
	
	// end of main controll program
		
	// Debugging
	initial begin
		// lets see some initial constant
		$display("ARRAY_SIZE = %d",`ARRAY_SIZE);	
		$display("RECEIVE_BUFFER_OFFSET = %d",`RECEIVE_BUFFER_OFFSET);
		$display("RECEIVE_BUFFER_SIZE = %d",`RECEIVE_BUFFER_SIZE);
		$display("SEND_BUFFER_SIZE = %d",`SEND_BUFFER_SIZE);		
	end

endmodule
