//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:14:04 09/23/2015 
// Design Name: 
// Module Name:    ResetButton 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Button #(
	parameter CNT_SIZE = 5
	)(
	input 	i_clk,
	input		i_bt,
	output 	o_bt
);

// register declaration
reg [CNT_SIZE-1:0]	r_static_cnt	=	{CNT_SIZE{1'b0}};

reg r_bt_now 	= 	1'b1;
reg r_bt_last 	= 	1'b1;
reg r_bt			=	1'b1;

// assign for output signal
assign	o_bt		=	r_bt;

// ------- debouncing for button --------
always @(posedge i_clk) begin
	// update button value
	r_bt_last 	= r_bt_now;
	r_bt_now 	= i_bt;
	
	if(r_bt_now == r_bt_last )begin	// button is temporally static
		if(r_static_cnt < {CNT_SIZE{1'b1}}) begin
			r_static_cnt = r_static_cnt + 1'b1;
		end else begin
			r_bt = r_bt_now;
		end
	end else begin	// button is pressed or released or bouncing
		r_static_cnt = {CNT_SIZE{1'b0}};
	end	
end
// ------ end of button debouncing -----
endmodule
