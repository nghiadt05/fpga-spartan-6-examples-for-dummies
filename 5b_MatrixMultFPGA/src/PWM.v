module PWM #(
		 parameter CTR_PWM_LEN = 10
	 )(
		 input							i_clk,
		 input							i_rst,
		 input	[CTR_PWM_LEN-1:0]	i_T_ON,
		 input	[CTR_PWM_LEN-1:0]	i_T_OFF,
		 output 							o_pwm
 );


reg [CTR_PWM_LEN-1:0] 	r_Cnt_T_ON_q 	= {CTR_PWM_LEN{1'b0}};
reg [CTR_PWM_LEN-1:0] 	r_Cnt_T_ON_d 	= {CTR_PWM_LEN{1'b0}};

reg [CTR_PWM_LEN-1:0]	r_Cnt_T_OFF_q 	= {CTR_PWM_LEN{1'b0}};
reg [CTR_PWM_LEN-1:0]	r_Cnt_T_OFF_d 	= {CTR_PWM_LEN{1'b0}};

reg r_pwm_d = 1'b0;
reg r_pwm_q = 1'b0;

assign o_pwm =  r_pwm_q;

// please remember that:
// + Do not use the incomplete "if" or "case" statements
// + Do consider about the combinatorial loop made by the same signals
//		e.g. if(x<10) x = x + 1; in this case do not use r_pwm_d in the if statement
always @(*) begin
	if( r_pwm_q && (r_Cnt_T_ON_q < i_T_ON) ) begin	//on
		r_pwm_d 			= 1'b1;		
		r_Cnt_T_ON_d 	= r_Cnt_T_ON_q + 1'b1;
		r_Cnt_T_OFF_d 	= {CTR_PWM_LEN{1'b0}};
	end else if( r_pwm_q && (r_Cnt_T_ON_q >= i_T_ON) ) begin	// on -> off
		// reset and change to off mode
		r_pwm_d 			= 1'b0;		
		r_Cnt_T_ON_d 	= {CTR_PWM_LEN{1'b0}};
		r_Cnt_T_OFF_d 	= {CTR_PWM_LEN{1'b0}};
	end else if( !r_pwm_q && (r_Cnt_T_OFF_q < i_T_OFF) ) begin	// off
		r_pwm_d 			= 1'b0;		
		r_Cnt_T_ON_d 	= {CTR_PWM_LEN{1'b0}};
		r_Cnt_T_OFF_d 	= r_Cnt_T_OFF_q + 1'b1;
	end else if( !r_pwm_q && (r_Cnt_T_OFF_q >= i_T_OFF) ) begin	// off -> on
		// reset and change to on mode
		r_pwm_d 			= 1'b1;		
		r_Cnt_T_ON_d 	= {CTR_PWM_LEN{1'b0}};
		r_Cnt_T_OFF_d 	= {CTR_PWM_LEN{1'b0}};
	end else begin	// default state (never get here)
		r_pwm_d 			= 1'b0;		
		r_Cnt_T_ON_d 	= {CTR_PWM_LEN{1'b0}};
		r_Cnt_T_OFF_d 	= {CTR_PWM_LEN{1'b0}};
	end
end

always @(posedge i_clk) begin
	if(!i_rst) begin		
		r_Cnt_T_ON_q 	<= {CTR_PWM_LEN{1'b0}};
		r_Cnt_T_OFF_q 	<= {CTR_PWM_LEN{1'b0}};
		r_pwm_q			<= 1'b0;
	end else begin		
		r_Cnt_T_ON_q	<= r_Cnt_T_ON_d;
		r_Cnt_T_OFF_q 	<= r_Cnt_T_OFF_d;		
		r_pwm_q 			<= r_pwm_d;	
	end	
end

endmodule
