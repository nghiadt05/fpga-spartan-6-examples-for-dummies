`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:54:22 09/23/2015
// Design Name:   Spartan6_TopModule
// Module Name:   D:/Data/CAPP/FPGA_Spartan6/Projects/ResetButton/src/TestTopModule.v
// Project Name:  ResetButton
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Spartan6_TopModule
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TestTopModule;

	// Inputs
	reg i_clk = 0;
	reg i_rs_bt = 1;
	reg [3:0] i = 0;
	
	// Outputs	
	wire o_rst;
	wire [3:0] o_led;

	// Instantiate the Unit Under Test (UUT)
	Spartan6_TopModule uut (
		.i_clk(i_clk), 
		.i_rs_bt(i_rs_bt), 		
		.o_rst(o_rst),
		.o_led(o_led)
	);

	initial begin
		// Initialize Inputs
		i_clk = 0;		
		i_rs_bt = 1;
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here		
		
		// reset button test		
			// initial state
			i_rs_bt = 1;
			// push reset button ...
			for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
				#2 i_rs_bt = ~i_rs_bt;
			end
			i_rs_bt = 0; 
			#200; // ... for a while
			
			// release reset button
			for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
				#2 i_rs_bt = ~i_rs_bt;
			end
			#2 i_rs_bt = 1;			
		// end of reset button test
		
	end
	
	always begin
		#1 i_clk = !i_clk;
	end
      
endmodule

