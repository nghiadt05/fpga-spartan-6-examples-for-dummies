`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:54:22 09/23/2015
// Design Name:   Spartan6_TopModule
// Module Name:   D:/Data/CAPP/FPGA_Spartan6/Projects/ResetButton/src/TestTopModule.v
// Project Name:  ResetButton
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Spartan6_TopModule
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TestTopModule;

	// Inputs
	reg i_clk = 0;
	reg i_rs_bt = 0;	
	reg [3:0] i_sw = 0;		
	
	// Outputs
	wire 			o_rst;
	wire [3:0] 	o_sw_stt;
	wire [3:0] 	o_led;
	
	// initial variables
	reg [3:0] i = 0;
	
	parameter CLK_RATE = 20_000_000; //hz
	parameter CTR_PWM_LEN = 5;

	// Instantiate the Unit Under Test (UUT)
	Spartan6_TopModule #(
		.CLK_RATE(CLK_RATE),
		.CTR_PWM_LEN(CTR_PWM_LEN)
	)
	uut (
		.i_clk(i_clk), 
		.i_rs_bt(i_rs_bt), 
		.i_sw(i_sw),
		.o_sw_stt(o_sw_stt),
		.o_led(o_led),
		.o_rst(o_rst)
	);

	initial begin
		// Initialize Inputs
		i_clk = 0;
		i_rs_bt = 1;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here		
		
		// reset button test		
			// initial state
			i_rs_bt = 1;	
			#200;
			// push reset button ...
			for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
				#2 i_rs_bt = ~i_rs_bt;
			end
			i_rs_bt = 0; 
			#200; // ... for a while
			
			// release reset button
			for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
				#2 i_rs_bt = ~i_rs_bt;
			end
			#2 i_rs_bt = 1;			
		// end of reset button test
		
		// DIP switch test
		for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
				#20 i_sw = i;
		end
		#200;
		for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
				#50 i_sw = i;
		end
		// end of DIP switch test
		
		
		
	end
	
	always begin
		#1 i_clk = !i_clk;
	end
      
endmodule

