`include "config.v"

module Blinker#(
	 parameter CLK_RATE = 20_000_000, //hz 
	 parameter SCALE = `NORMAL
	 )(	 
	 input  	i_clk,
	 input	i_rst,
	 output 	o_led
);

parameter CNT_SIZE = $clog2(CLK_RATE*SCALE);

reg r_led = 1'b0;
reg [CNT_SIZE-1:0] r_led_counter = {CNT_SIZE{1'b0}};

assign o_led = r_led;

always @(posedge i_clk) begin	
	if(!i_rst)begin
		r_led_counter <= {CNT_SIZE{1'b0}};
		r_led			  <= 1'b0;
	end else begin
		r_led_counter <= r_led_counter + 1'b1;
		r_led			  <= r_led_counter[CNT_SIZE-1];
	end
end

/*
	debug
*/
//initial begin
//	$display("CLK_RATE = %d -- SCALE= %0.1f -- CNT_SIZE= %d -- CNT_MAX= %d",CLK_RATE, SCALE, CNT_SIZE, {CNT_SIZE{1'b1}});
//end 

endmodule
