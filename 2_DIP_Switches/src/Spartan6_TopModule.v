//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:46:21 09/21/2015 
// Design Name: 
// Module Name:    Spartan6_TopModule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "config.v"

module Spartan6_TopModule #(
	parameter CLK_RATE = 20_000_000, //hz
	parameter CTR_PWM_LEN = 5
	)(
	// global signals
	input 	i_clk,
	input		i_rs_bt,
	output 	o_rst,
	// --------------
	
	// signals for Switch module
	input		[3:0]	i_sw, 
	output	[3:0] o_sw_stt,
	// -------------------------
	
	// signals for LEDs module
	output 	[3:0] o_led
	// -------------------------
);

	// mapping signals for reset button module
	parameter BUTTON_CNT_SIZE = 3;
	Button #(
		.CNT_SIZE(BUTTON_CNT_SIZE)
	)Reset_Button(
		.i_clk(i_clk),
		.i_bt(i_rs_bt),
		.o_bt(o_rst)
	);
	// ---------------------------------------
	
	// mapping signals for DIP switches module
	// assume that each DIP switch is a button	
	DIP_Switches #(
		.CNT_SIZE(BUTTON_CNT_SIZE)
	)DIP_Switches(		
		.i_clk(i_clk),
		.i_rst(o_rst),
		.i_sw(i_sw),
		.o_sw_stt(o_sw_stt)
	);	
	// ---------------------------------------
	
	// assign DIP switch values for the LEDs
	assign o_led = o_sw_stt;
	// ---------------------------------------

endmodule
