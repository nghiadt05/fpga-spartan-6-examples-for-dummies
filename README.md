# README #

This project contains fundamental examples for an FPGA kit constructed from the Spartan 6.

Here are the contents of the example set:
1. Reset button
2. DIP switches
3. PWM/LEDS blink
4. UART
5a. FPGA-Arduino UART communication
5b. FPGA-Arduino Matrix multiplication  


Please read the manual for more detail instructions.    