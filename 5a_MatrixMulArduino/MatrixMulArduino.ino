/*
    Computer Architecture and Parallel Processing LAB

- Brief description: Matrix multiplication using Arduino and FPGA

- Hardware connection:

*/
#include <SoftwareSerial.h>
#include <Stdint.h>
#include <avr/pgmspace.h>

#define FPGA_T 5e-6 // uS

#define START                 0
#define PREPARE_AND_SEND      1
#define RECEIVE               2
#define CALCULATE_COMPARE     3

#define ARRAY_SIZE            5
#define MATRIX_SIZE           ARRAY_SIZE*ARRAY_SIZE
#define SEND_BUFFER_OFFSET    ARRAY_SIZE*4
#define SEND_BUFFER_SIZE      2*ARRAY_SIZE*4   + 1   // ( 2 arrays * N elements )  * 4 bytes + 1 check sume byte 
#define RECEIVE_BUFFER_SIZE   (1+1)*4 + 1            // ( 1 result value + 1 timming value ) * 4 bytes + 1 check sume byte

#define REQUEST_TIME_OUT      1000 // in miliseconds

// Change the SoftwareSerial library file to un-pull RX pin to 5V
// It's important since software serial pulls RX pin to 5V, that will kill our FPGA immediately.
SoftwareSerial SoftSerial(10, 11); // RX, TX

uint32_t  A_SumProduct;
uint32_t  F_Result;

uint32_t A_Matrix [MATRIX_SIZE];
uint32_t F_Matrix [MATRIX_SIZE];

uint8_t   RECEIVE_BUFFER  [RECEIVE_BUFFER_SIZE];
uint8_t   STATE_ID = START;

uint32_t  Start_Time = 0;
uint32_t  Arduino_Time = 0;
float F_Time = 0.0f;

const int A_RTS = 9;  // output to FPGA
const int F_CTS = 8;  // input from FPGA, without pulling up

const int A_CTS = 12; // output to FPGA
const int F_RTS = 13; // input without pulling up

uint16_t element_cnt = 0;
uint8_t recv_cnt = 0;
bool Correctness = true;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(250000);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // set the data rate for the SoftwareSerial port
  pinMode(10, INPUT);
  SoftSerial.begin(2400);

  // Initialize the random generator of Arduino
  randomSeed(analogRead(0));

  // Initialize GPIO pins
  pinMode(A_RTS, OUTPUT);
  pinMode(F_CTS, INPUT);

  pinMode(A_CTS, OUTPUT);
  pinMode(F_RTS, INPUT);

  //   digitalWrite(A_CTS, HIGH);

  // Debug initialization data
  Serial.println("Send '1' via USART to start !");
  //  Serial.print("SEND_BUFFER_SIZE = "); Serial.println(SEND_BUFFER_SIZE);
  //  Serial.print("RECEIVE_BUFFER_SIZE = "); Serial.println(RECEIVE_BUFFER_SIZE);
  //  Serial.print("SEND_BUFFER_OFFSET = "); Serial.println(SEND_BUFFER_OFFSET);
  Serial.println();
}

void loop() {
  if (STATE_ID == START) {
    recv_cnt = 0;
    Correctness = true;
    element_cnt = 0;
    Arduino_Time = 0;
    F_Time = 0.0f;
    digitalWrite(A_RTS, LOW);
    digitalWrite(A_CTS, LOW);

    if ( Serial.available() ) {
      uint8_t tmp = Serial.read();
      if ( tmp == '1') {
        STATE_ID = PREPARE_AND_SEND;
        Serial.println("Processing ...");
      }
    }
  }
  else if (STATE_ID == PREPARE_AND_SEND) {
    uint32_t  A[ARRAY_SIZE];
    uint32_t  B[ARRAY_SIZE];
    uint8_t   SEND_BUFFER[SEND_BUFFER_SIZE];
    uint8_t   CHKSUM = 0;

    // Generate data for two arrays then assign data to send buffer
    for (int i = 0; i < ARRAY_SIZE; i++) // for every element of an NxN array
    {
      uint8_t byteA[4], byteB[4];
      //A[i] = random(5);
      //B[i] = random(5);
      A[i] = random(100);
      B[i] = random(100);
      //Serial.print(A[i]); Serial.print("  "); Serial.println(B[i]);

      byteA[3] = (A[i] >> 24)  & 0xff; CHKSUM ^= byteA[3];
      byteA[2] = (A[i] >> 16)  & 0xff; CHKSUM ^= byteA[2];
      byteA[1] = (A[i] >> 8)   & 0xff; CHKSUM ^= byteA[1];
      byteA[0] = (A[i])        & 0xff; CHKSUM ^= byteA[0];

      byteB[3] = (B[i] >> 24)  & 0xff; CHKSUM ^= byteB[3];
      byteB[2] = (B[i] >> 16)  & 0xff; CHKSUM ^= byteB[2];
      byteB[1] = (B[i] >> 8)   & 0xff; CHKSUM ^= byteB[1];
      byteB[0] = (B[i])        & 0xff; CHKSUM ^= byteB[0];

      uint16_t segment = 4 * i;

      SEND_BUFFER[segment + 0] = byteA[0];
      SEND_BUFFER[segment + 1] = byteA[1];
      SEND_BUFFER[segment + 2] = byteA[2];
      SEND_BUFFER[segment + 3] = byteA[3];

      SEND_BUFFER[SEND_BUFFER_OFFSET + segment + 0] = byteB[0];
      SEND_BUFFER[SEND_BUFFER_OFFSET + segment + 1] = byteB[1];
      SEND_BUFFER[SEND_BUFFER_OFFSET + segment + 2] = byteB[2];
      SEND_BUFFER[SEND_BUFFER_OFFSET + segment + 3] = byteB[3];
    }
    SEND_BUFFER[SEND_BUFFER_SIZE - 1] = CHKSUM;
    //Serial.println("Finish generating matrix.");
    // end of genrate arrays

    // Do the sum product using Arduino
    Start_Time = micros();
    A_SumProduct = 0;
    for ( uint8_t i = 0; i < ARRAY_SIZE; i++) {
      A_SumProduct = A_SumProduct + (A[i] * B[i]);
    }
    Arduino_Time += micros() - Start_Time;

    // end of matrix multiplication

    // Send data to the FPGA board
    for ( uint16_t i = 0; i < SEND_BUFFER_SIZE ; i++)
    {
      // signal RTS
      digitalWrite(A_RTS, HIGH);
      // wait for CTS
      uint32_t tmp_time_now = millis();
      uint32_t tmp_eslape_time = 0;
      while (!digitalRead(F_CTS))
      {
        tmp_eslape_time = millis() - tmp_time_now;
        if ( tmp_eslape_time > REQUEST_TIME_OUT )break;
      }
      // send a data byte if connection is ok
      if (tmp_eslape_time <= REQUEST_TIME_OUT)
      {
        SoftSerial.write(SEND_BUFFER[i]);
        delayMicroseconds(10);
        //Serial.println(SEND_BUFFER[i]);
        digitalWrite(A_RTS, LOW);
      }
      else
      {
        digitalWrite(A_RTS, LOW);
        Serial.println("Check for connection, returning to START state");
        STATE_ID = START;
        break;
      }
    }
    // finish sending data
    STATE_ID = RECEIVE;
    //STATE_ID = START;//RECEIVE;
    //Serial.println("Move to RECEIVE state");
  }
  else if (STATE_ID == RECEIVE) {
    while(recv_cnt < RECEIVE_BUFFER_SIZE)
    {
      uint32_t tmp_eslape_time = 0;
      if (digitalRead(F_RTS)) { // FPGA board data ready signal
        uint32_t tmp_time_now = millis();       
        digitalWrite(A_CTS, HIGH);        
        while (!SoftSerial.available()){
          tmp_eslape_time = millis() - tmp_time_now;
          if ( tmp_eslape_time > REQUEST_TIME_OUT )break;
        }
        if(tmp_eslape_time < REQUEST_TIME_OUT){
          RECEIVE_BUFFER[recv_cnt] = SoftSerial.read();
          digitalWrite(A_CTS, LOW);
          //Serial.println(recv_cnt);
          recv_cnt = recv_cnt + 1;
          if (recv_cnt == RECEIVE_BUFFER_SIZE)
          {
            STATE_ID = CALCULATE_COMPARE;
            //Serial.println("Move to CALCULATE_COMPARE state");
          }
        }
      }
      if (tmp_eslape_time >= REQUEST_TIME_OUT )
      {
        Serial.println("Cant receive data from FPGA, return to IDLE state");
        Serial.println("Send '1' via USART to start !");
        STATE_ID = START;
        break;
      }
    }
  }
  else if (STATE_ID == CALCULATE_COMPARE) {
    uint32_t F_SumProduct = (RECEIVE_BUFFER[3] << 24) |
                            (RECEIVE_BUFFER[2] << 16) |
                            (RECEIVE_BUFFER[1] << 8) |
                            (RECEIVE_BUFFER[0]);

    uint32_t COUNT =  (RECEIVE_BUFFER[7] << 24) |
                      (RECEIVE_BUFFER[6] << 16) |
                      (RECEIVE_BUFFER[5] << 8) |
                      (RECEIVE_BUFFER[4]);

    F_Time += COUNT * FPGA_T;
    if (F_SumProduct != A_SumProduct) {
      Correctness = false;
      Serial.print(" Wrong ----------------> ");
      Serial.print(F_SumProduct);
      Serial.print(" ");
      Serial.println(A_SumProduct);
    }
    else {
      Serial.print(F_SumProduct);
      Serial.print(" ");
      Serial.println(A_SumProduct);
    }
    A_Matrix[element_cnt] = A_SumProduct;
    F_Matrix[element_cnt] = F_SumProduct;


    element_cnt ++;
    if (element_cnt < MATRIX_SIZE) {
      digitalWrite(A_RTS, LOW);
      digitalWrite(A_CTS, LOW);
  
      recv_cnt = 0;
      STATE_ID = PREPARE_AND_SEND;
      delay(300);
    }
    else {
      Serial.println();
      Serial.println("A_MAtrix: ");
      for (int i = 0; i < MATRIX_SIZE; i++)
      {
        if ( (i % ARRAY_SIZE) == 0 && i > 0) {
          Serial.println();
        }
        Serial.print(A_Matrix[i]);
        Serial.print("  ");
      }
      Serial.println();
      Serial.println();
      Serial.println("F_Matrix: ");
      for (int i = 0; i < MATRIX_SIZE; i++)
      {
        if ( (i % ARRAY_SIZE) == 0 && i > 0 ) {
          Serial.println();
        }
        Serial.print(F_Matrix[i]);
        Serial.print("  ");
      }
      Serial.println();

      if (!Correctness) {
        Serial.println("Wrong calculation");
      }
      else if (Correctness == true && F_Time < Arduino_Time) {
        Serial.println();
        Serial.print("Matrix mult. in Arduino takes: ");Serial.print(Arduino_Time);Serial.println("  (us)");
        Serial.print("Matrix mult. in FPGA takes: ");Serial.print(F_Time);Serial.println("  (us)");
        Serial.print("Same results and FPGA board is ");
        Serial.print(Arduino_Time / F_Time);
        Serial.println(" times faster than Arduino.");
      }
      Serial.println("Send '1' via USART to start !");
      STATE_ID = START;
    }
  }
}



























