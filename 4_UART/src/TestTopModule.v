`timescale 1ns/1ps
module TestTopModule;

	// Inputs
	reg i_clk = 0;
	reg i_rs_bt = 1;
	reg [3:0] i = 0;
	reg [3:0] i_sw = 4'b0;
	
	// Outputs
	wire o_rst;
	wire [3:0] o_sw_stt;
	wire o_tx;
	wire [3:0] o_led;
	
	// Instantiate the Unit Under Test (UUT)
	Spartan6_TopModule uut (
		.i_clk(i_clk), 
		.i_rs_bt(i_rs_bt), 
		
		.i_sw(i_sw), 
		.o_sw_stt(o_sw_stt),
		
		.o_led(o_led),
		
		.o_tx(o_tx),
		
		.o_rst(o_rst)
	);

	initial begin
		// Initialize Inputs
		i_clk = 0;		
		i_rs_bt = 1;
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here		
		
		// switch button test
//		for (i=0; i<4'b1111; i=i+1)begin
//			#100 i_sw = i;
//		end
		// end of switch button test
		
//		// reset button test		
//			// initial state
//			i_rs_bt = 1;
//			// push reset button ...
//			for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
//				#2 i_rs_bt = ~i_rs_bt;ooooo
//			end
//			i_rs_bt = 0; 
//			#200; // ... for a while
//			
//			// release reset button
//			for ( i=0; i<4'b1111 ; i=i+1 )begin	// bouncing
//				#2 i_rs_bt = ~i_rs_bt;
//			end
//			#2 i_rs_bt = 1;			
//		// end of reset button test
		
	end
	
	always begin
		#1 i_clk = !i_clk;
	end
      
endmodule