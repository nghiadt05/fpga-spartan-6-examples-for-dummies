module Serial_tx #(
        parameter CLK_PER_BIT = 50
    )(
        input 	clk,			// input clock
        input 	rst,			// input reset signal
        output tx,			// output tx signal
        input 	block,		// block signal sent by outer source to stop transmission(AVR, ...)
        output busy,			// output signal to indicate the UART is busy
        input 	[7:0] data,	// input signals that indicate the sending data
        input 	new_data		// signal to indicate new data is comming
    );

    // clog2 is 'ceiling of log base 2' which gives you the number of bits needed to store a value
    parameter CTR_SIZE = $clog2(CLK_PER_BIT);

	 // Listing all possible stages
    localparam STATE_SIZE = 2;
    localparam IDLE = 2'd0,
    START_BIT = 2'd1,
    DATA = 2'd2,
    STOP_BIT = 2'd3;

	 
    reg [CTR_SIZE-1:0] ctr_d, ctr_q;	// bit length counter
    reg [2:0] bit_ctr_d, bit_ctr_q;
    reg [7:0] data_d, data_q;
    reg [STATE_SIZE-1:0] state_d, state_q = IDLE;
    reg tx_d, tx_q;
    reg busy_d, busy_q;
    reg block_d, block_q;

    assign tx = tx_q;
    assign busy = busy_q;
	
    always @(*) begin
        block_d = block;
        ctr_d = ctr_q;		
        bit_ctr_d = bit_ctr_q;
        data_d = data_q;
        state_d = state_q;
        busy_d = busy_q;
        case (state_q)
				// initialized state 
            IDLE: begin	
                if (block_q) begin				// block UART transmitting function
                    busy_d = 1'b1;
                    tx_d = 1'b1;
                end else begin
                    busy_d = 1'b0;
                    tx_d = 1'b1;
                    bit_ctr_d = 3'b0; 			// bit index of the sending byte
                    ctr_d = 1'b0;	  			// bit length counter
                    if (new_data) begin		// if new data is ready to be sent
                        data_d = data;			// load this data first
                        state_d = START_BIT;	// move to the next stage
                        busy_d = 1'b1;			// signal this flag since the TX module is now busy for transmitting data
                    end
                end
            end
				// send a start bit
            START_BIT: begin						
					 f= 20Mhz => t = 1/f
					 BR = 9600 bits/sec
					 t_1_bit = 1/9600 sec
					 clock_per_bit = t_1_bit / t = f/BR = 2083.33 clocks
					 => Counter size for the length of a bit is: ceil(log2(clock_per_bit)) = 12 bits
                busy_d = 1'b1;					// i'm still busy
                ctr_d = ctr_q + 1'b1;			// increase the bit length counter
                tx_d = 1'b0;						// send start signal as long as the time defined by the Baudrate
                if (ctr_q == CLK_PER_BIT - 1) begin
                    ctr_d = 1'b0;				// reset this counter for the next data byte
                    state_d = DATA;				// move to the next state
                end
            end
				// send each data bit in the input byte via tx line
            DATA: begin
                busy_d = 1'b1;					// i'm still busy now
                tx_d = data_q[bit_ctr_q];		// send data from data[0] to data[7], each bit need the appropriate length indicated by the Baudrate
                ctr_d = ctr_q + 1'b1;			// increase the bit length counter
                if (ctr_q == CLK_PER_BIT - 1) begin	// bit duration is long enought
                    ctr_d = 1'b0;							// reset the bit length counter
                    bit_ctr_d = bit_ctr_q + 1'b1;		// move to the next bit
                    if (bit_ctr_q == 7) begin			// if it's the final bit, then move to the next state
                        state_d = STOP_BIT;
                    end
                end
            end
				// send a stop bit via rx line
            STOP_BIT: begin						
                busy_d = 1'b1;					// well, i've not finished yet, thit is the last thing i have to send
                tx_d = 1'b1;						// pull this line up to indicate the stop signal
                ctr_d = ctr_q + 1'b1;			// increase the bit length counter
                if (ctr_q == CLK_PER_BIT - 1) begin	// after finishing sending a byte, move to the idle state
                    state_d = IDLE;				
                end
            end
            default: begin
                state_d = IDLE;					
            end
        endcase
    end

	 // synchronous update state status 
    always @(posedge clk) begin
        if (!rst) begin
            state_q <= IDLE;
            tx_q <= 1'b1;
        end else begin
            state_q <= state_d;
            tx_q <= tx_d;
        end

        block_q <= block_d;	
        data_q <= data_d;
        bit_ctr_q <= bit_ctr_d;
        ctr_q <= ctr_d;
        busy_q <= busy_d;
    end

endmodule