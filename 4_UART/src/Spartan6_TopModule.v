//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:46:21 09/21/2015 
// Design Name: 
// Module Name:    Spartan6_TopModule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module Spartan6_TopModule #(
	parameter CLK_RATE = 20_000_000, //hz
	parameter CTR_PWM_LEN = 5
	)(
	// global signals
	input 	i_clk,
	input		i_rs_bt,
	output 	o_rst,
	// --------------
	
	// signals for Switch module
	input		[3:0]	i_sw, 
	output	[3:0] o_sw_stt,
	// -------------------------
	
	// signals for UART module
	output 	o_tx,	// output tx signal	
	input 	i_rx, // input rx signal
	// -------------------------
			
	// signals for LEDs module (in this example outputs of PWM module are the LEDS)
	output 	[3:0] o_led
	// -------------------------	
);
	// Cnt = 2^BUTTON_CNT_SIZE clock cycles for button debouncing
	parameter BUTTON_CNT_SIZE = 5; 
	
	// mapping signals for reset button module	
	Button #(
		.CNT_SIZE(5)		
	)Reset_Button(
		.i_clk(i_clk), 
		.i_bt(i_rs_bt),
		.o_bt(o_rst)
	);
	// ---------------------------------------
	
	// preparing and mapping signals for USART module	
	wire 	tx_busy;				// output signal to indicate the UART is busy
	wire 	[7:0] tx_data;		// input signals that indicate the sending data
	reg 	tx_new_data;		// signal to indicate new data is ready to send via tx line
	
	wire 	rx_new_data;		// signal to indicate new data is ready to read via rx line
	wire	[7:0] rx_data;		// data read from RX module
	
	USART #(
		.CLK_RATE(CLK_RATE),
		.SERIAL_BAUD_RATE(9600)
	) USART_1 (
		// global signals
		.i_clk(i_clk),
		.i_rst(o_rst),

		// for TX module 
		.tx(o_tx),						// output tx signal
		.block(1'b0),						// block signal sent by outer source to stop transmission(AVR, ...)
		.tx_busy(tx_busy),			// output signal to indicate the UART is busy
		.tx_data(tx_data),			// input signals that indicate the sending data
		.tx_new_data(tx_new_data),	// signal to indicate new data is comming
		
		// for RX module
		.rx(i_rx),						// input serial rx line for RX module
		.rx_data(rx_data),			// output data read from RX module
		.rx_new_data(rx_new_data)	// output signal that indicates data is ready for a read
	);
	
	// read data from ROM module then send it via UART line
	localparam STATE_SIZE = 1;
	localparam 	IDLE = 0,
					PRINT_MESSAGE = 1;
	localparam MESSAGE_LEN = 14;

	reg [STATE_SIZE-1:0] state_d, state_q;
	reg [3:0] addr_d, addr_q;
	
	Message_rom Message_rom(
		.clk(i_clk),
		.addr(addr_q),
		.data(tx_data)
	);
	
	always @(*) begin
		state_d = state_q;
		addr_d = addr_q;
		tx_new_data = 1'b0;
		
		case (state_q)
			IDLE :			begin
									addr_d = 4'b0;
									// wait for print message order
									if(rx_new_data && rx_data=="1")begin
										state_d = PRINT_MESSAGE;
									end
									// ----------------
									//state_d = PRINT_MESSAGE;
								end
								
			PRINT_MESSAGE: begin
									if(!tx_busy)begin
										tx_new_data = 1'b1;
										addr_d = addr_q + 1'b1;
										if(addr_q == MESSAGE_LEN)begin
											addr_d = 4'b0;
											state_d = IDLE;
										end									
									end
								end
								
			default: 		begin
									state_d = IDLE;
								end
		endcase
	end
	
	always @(posedge i_clk) begin
		if(!o_rst) begin
			state_q <= IDLE;
		end else begin
			state_q <= state_d;
		end
		addr_q <= addr_d;
	end
	
	// assign led[0] to tx
	assign o_led[0] = !o_tx;
	// assign led[1] to tx
	assign o_led[1] = !i_rx;
	// ---------------------------------------
		
endmodule




