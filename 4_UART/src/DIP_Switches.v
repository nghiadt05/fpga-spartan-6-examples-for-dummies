module DIP_Switches #(
	parameter CNT_SIZE = 5
	)(	 
	input 			i_clk,
	input				i_rst,
	input 	[3:0]	i_sw,
	output	[3:0]	o_sw_stt		 
);

// read the input of the DIP switches and button
// then pass this values to sw_bt_stt [4:0]
// sw_stt[3]: sw[3] 	status
// sw_stt[2]: sw[2] 	status
// sw_stt[1]: sw[1] 	status
// sw_stt[0]: sw[0] 	status	

	wire [3:0] o_sw_stt_tmp ;//= 4'b1111;

	// initially, the voltage level of all buttons are high
	assign o_sw_stt[0] = o_sw_stt_tmp[0] | (~i_rst);
	assign o_sw_stt[1] = o_sw_stt_tmp[1] | (~i_rst);
	assign o_sw_stt[2] = o_sw_stt_tmp[2] | (~i_rst);
	assign o_sw_stt[3] = o_sw_stt_tmp[3] | (~i_rst);

	Button #(
		.CNT_SIZE(CNT_SIZE)
		)DIP_SW_0(
		.i_clk(i_clk),
		.i_bt(i_sw[0]),
		.o_bt(o_sw_stt_tmp[0])
	);

	Button #(
		.CNT_SIZE(CNT_SIZE)
		)DIP_SW_1(
		.i_clk(i_clk),
		.i_bt(i_sw[1]),
		.o_bt(o_sw_stt_tmp[1])
	);

	Button #(
		.CNT_SIZE(CNT_SIZE)
		)DIP_SW_2(
		.i_clk(i_clk),
		.i_bt(i_sw[2]),
		.o_bt(o_sw_stt_tmp[2])
	);

	Button #(
		.CNT_SIZE(CNT_SIZE)
		)DIP_SW_3(
		.i_clk(i_clk),
		.i_bt(i_sw[3]),
		.o_bt(o_sw_stt_tmp[3])
	);

endmodule
